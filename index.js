console.log("hello, world!");

// [SECTION] Functions
// Function isn javascript are lines/block of codes that tell our device/application ot performa cetain tast when call/invoked.
// They are alos used to prevent repeating lines.block of codes that perfome the same task/functions.

// functon Declaration 
 		// function statement defines a fuction with parameters

 		// function keyword - use to define a javascript function
 		// function name - is used tso we are able to call/invoke a declared function 
 		// function code block ({}) - the statement which compromise the body of the ffuncytion.. this is twhere the code to be executed  

// function declaration //function name
// function name requires open and close parenthesis beside it
 		function printName() { // code block
 		console.log("My Name is John") // function statement
 		}; //delimeter

printName(); // function Invocation


// [HOSTING]
//  Hoising is Javascript's  behavior for centain variables and functions to run or use before their declaration.
declaredFunction();
// make sure the function is exsiting whenever we call/invoke a function

	function declaredFunction (){
		console.log("Hello world");
	}

declaredFunction();

// [Function Expression]
// A function can be also store in a variable. that is called as function expression
// Hoisting is allowed in function declaration, however, we cound not hoist through function
let variableFunction = function(){
	console.log("hello again!");
};


variableFunction();

let funcExpression = function funcName() {
	console.log("hello from the other side");
};

funcExpression();

// funcName();	
// ERROR: whenever we are calling a named function stored in variable, we just call the variable name, not the function name

// Error: we just call the variable name, not the function name

console.log("---------------------");
console.log("[Reasssigning Function]");

declaredFunction();
declaredFunction = function() {
	console.log("updated declaredFunction");
}

declaredFunction();


funcExpression = function (){
	console.log("updated funcExpression");

}

funcExpression();




//  Constatant Function

const constantFuction = function() {
	console.log("Initialized with const.");
}

constantFuction();

/* 
	constantFuction = function(){
	console.log("New value");

}

constantFuction();

*/

// function with const keyword cannot be reassigned.
// scope is the accessibility/visibility of a variables in the code

/* 
	JS variables has 3 scopes
	1. local scope
	2. global scope
	3. function scope

*/


console.log("---------------------");
console.log("[Function Scoping]");


{

	let localVar = "Armando Perez";
	console.log(localVar);
}

 // console.log(localVar);


let globalVar = "Mr. Worldwide";
console.log(globalVar);


{
console.log(globalVar);
}


// [Function Scoping]
// Variables define inside a function are not accessible/visible outside the function 
// Variable declared with var, let and const are quite similar when declared inside a function
function showNames(){
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();


// // Error - This are function scoped variable anf cannot be acced outside the function they were declared in 
// 	console.log(functionVar);
// 	console.log(functionConst);
// 	console.log(functionLet);


function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John"
		console.log(name);
	}

	nestedFunction();
	console.log(nestedFunction);

}

myNewFunction();

// globa Scoped Variable
let globalName = "Zuitt";

function myNewFunction2(){
	let nameInside = "Renz";
	console.log(globalName);
}

myNewFunction2();

// alert() allows is to show a small window at the top of our browser page to show information to our user.
// alert("Sample Alert");

function showSampleAlert(){
	alert("Hello, User");
}

showSampleAlert();

// alert messsages insde a function will only execute whenever we call/invoke the function
console.log("I will only log in the console when the alert is dismissed");	

// Notes on the use of aler();
	 // show only an alert for short dialogs/messages to user 
	 // dDo not overuse alert() because the program/js has to wait for it to be dismissed before continuing.


// [Promt]
	// promt() alllow is to show small windw at the top of the browser to gather user input.

		// let samplePromt = prompt("Enter Your Full Name");

		// console.log(samplePromt);

		// console.log(typeof samplePromt);

		// console.log("Hello," + samplePromt);

		function printWelcomeMessage() {
			let firstName = prompt ("Enter your first name: ");
			let lastName = prompt("Enter your last name: ");

			console.log("hello," + firstName + "" + lastName + "!");
			console.log("Welcome to my page!")
		}

		printWelcomeMessage();

		function getCourses(){
			let courses = [ "Science 101", "math 101", "English 101"];
			console.log(courses);
		}

		getCourses();

		// Avoid generic names to avoid cofusion witthin our code.

		// [SECTION] Function Naming Convention

		// Function name should be definitive of the task it will perform. it usually contains a verb

		function get(){
			let name = "Jamie";
			console.log(name);
		}


		get();

		// avoid pointless and inappropriate function names, example: foo,bar, etc.

		function foo(){
			console.log(25%5);
		}

		foo();

		// Name your function in small caps. follow camelCase when naming variables and functions

		function displayCarInfo(){
			console.log ("Brand: Toyota");
			console.log	("brand: Sedan");
			console.log ("price: 1,500,000");
		}

		displayCarInfo();


